O:我今天学习了Tasking任务拆分，并且自己动手做了好几个任务拆分的任务还用编码去实现了任务，最后编码完成一个有难度的、经过拆分后的程序。
R:进步。
I:我认为最后对打印小票任务的编码有些复杂，需要时间去整理思路，与实习过的同学之间的差距非常明显。一开始老师演示编码时讲的太快了，让我的思维一下子变得混乱。今天学习的内容让我获益良多，体会到了任务拆分对完成庞大而复杂的项目有非常大的作用。
D:提升自己的代码编写能力。在遇见困难问题时使用Tasking去一步步完成。
