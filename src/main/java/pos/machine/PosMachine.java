package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        String[] barcodeList = new String[barcodes.size()];
        for (int i = 0; i < barcodeList.length; i++) {
            barcodeList[i] = barcodes.get(i);
        }
        List<ReceiptItem> receiptItems = this.decodeToItems(barcodeList);
        Receipt receipt = new Receipt(receiptItems);
        var receiptString = new StringBuilder();
        receiptString.append("***<store earning no money>Receipt***\n");
        for(ReceiptItem receiptItem : receipt.getReceiptItems()) {
            receiptString.append(receiptItem.toString());
        }
        receiptString.append("----------------------\n");
        var total = "Total: " + receipt.getTotalPrice() + " (yuan)\n";
        receiptString.append(total);
        receiptString.append("**********************");

        return receiptString.toString();
    }

    public List<ReceiptItem> decodeToItems(String[] barcodes) {
        TreeMap<String,Integer> barcodeMap = new TreeMap<String,Integer>();
        for (String barcode : barcodes) {
            if(barcodeMap.containsKey(barcode)) {
                barcodeMap.put(barcode,barcodeMap.get(barcode) + 1);
            }else{
                barcodeMap.put(barcode,1);
            }
        }

        var receiptItemList = new ArrayList<ReceiptItem>();
        for(String barcode : barcodeMap.keySet()) {
            Item item = this.findItemByBarcode(barcode);
            if(item != null) {
                receiptItemList.add(new ReceiptItem(item.getName(), barcodeMap.get(barcode), item.getPrice()));
            }
        }

        return receiptItemList;
    }

    public Item findItemByBarcode(String barcode) {
        List<Item> items = ItemsLoader.loadAllItems();
        for(Item item : items) {
            if(item.getBarcode().equals(barcode)) {
                return item;
            }
        }
        return null;
    }

}
